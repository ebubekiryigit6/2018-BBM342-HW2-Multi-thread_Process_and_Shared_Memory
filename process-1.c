/**
 *
 * Author:      Ebubekir Yigit
 *
 * ID:          21328629
 *
 * File:        process-1.c
 *
 * Purpose:     The purpose of the assignment; to develop a multiprocessing and multithread cipher that communicates with pipes.
 *              First process will create its own 5 threads and the last thread calls second process.
 *              Second process provides authentication.
 *              Each process writes outputs to files like process-<id>-thread-<id>.txt format.
 *              Each process will serve in separate ".c" files.
 *
 *
 *
 * Compile:
 *              gcc process-1.c -o process-1 -ansi -Wall -lpthread
 *              gcc process-2.c -o process-2 -ansi -Wall -lpthread
 *
 * Run:         ./process-1 ./sample-1/plain.txt ./sample-1/key.txt
 *
 *
 * Input:       None
 *
 * Output:      Please See -->  ./process-1-thread-0.txt    ./process-1-thread-1.txt    ./process-1-thread-2.txt
 *                              ./process-1-thread-3.txt    ./process-1-thread-4.txt
 *
 *                              ./process-2.txt
 *
 *
 *
 * Notes:       I copied the same functions as the header files and different ".c" files do not fit the submit format.
 *              Sorry for that.
 *
 *              !!!!
 *              IMPORTANT NOTE: I compile and run the code in Ubuntu. It works correctly,
 *                              but in dev server, sometimes outputs are wrong. I dont know why.
 *              !!!!
 *
 *              And also, Code has been tested with all of the shared sample files.
 *              All tests were successful, I hope code succeed when you try :) Good work.
 *
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define READ 0
#define WRITE 1

#define THREAD_COUNT 5
#define MUTEX_COUNT 4
#define QUEUE_COUNT 4

#define MAX_QUEUE_SIZE  5

#define END_OF_FILE_CONTROL "EOF\0"


typedef struct {
    char *string;
    int size;
    int top;
} String;

typedef struct {
    char **array;
    int front;
    int rear;
    int size;
} Queue;


void usage();

void initializeString(String *string, int initialSize);

void concatString(String *string, char *newCharArray);

void clearString(String *string);

void initializeQueue(Queue *queue, int initialSize);

void enqueue(Queue *queue, char *element);

char *dequeue(Queue *queue);

int isEmptyQueue(Queue *queue);

void *executeThreads(void *rank);

void thread_0();

void thread_1();

void thread_2();

void thread_3();

void thread_4();

void sendToQueueWithIndex(int index, char *element);

String getKey(char *keyFilePath);

void initGlobals();

int *convertIntArray(char *str);

String convertCharArray(int *array);

int xor(
int x,
int y
);

void swap(int *a, int *b);

unsigned char get_subbox_val(unsigned char val);


pthread_mutex_t mutex_locks[MUTEX_COUNT];
Queue thread_queues[QUEUE_COUNT];
pthread_t *thread_handles;

char *plain_text_path;
char *key_file_path;


int main(int argc, char **argv) {
    if (argc != 3) {
        usage();
        exit(EXIT_FAILURE);
    }

    initGlobals();
    key_file_path = argv[2];
    plain_text_path = argv[1];

    long thread_id;

    for (thread_id = 0; thread_id < THREAD_COUNT; thread_id++) {
        pthread_create(&thread_handles[thread_id], NULL, executeThreads, (void *) thread_id);
    }

    for (thread_id = 0; thread_id < THREAD_COUNT; thread_id++) {
        pthread_join(thread_handles[thread_id], NULL);
    }

    return EXIT_SUCCESS;
}

void *executeThreads(void *rank) {
    long my_rank = (long) rank;
    /* seperate threads to functions */

    if (my_rank == 0) {
        thread_0();
    } else if (my_rank == 1) {
        thread_1();
    } else if (my_rank == 2) {
        thread_2();
    } else if (my_rank == 3) {
        thread_3();
    } else if (my_rank == 4) {
        thread_4();
    }
    return NULL;
}

void thread_4() {
    int pipefd[2];
    pid_t pid_1;
    pipe(pipefd); /* create pipe for process0 & process1 */

    FILE *outFile;

    outFile = fopen("process-1-thread-4.txt", "w");

    pid_1 = fork();
    if (pid_1 == 0) {
        /* call process1 and send buffers via pipe */
        String readPipe;
        initializeString(&readPipe, 100);
        sprintf(readPipe.string, "%d", pipefd[READ]);
        execl("./process-2", readPipe.string, NULL); /* run process 1 binary file */
        printf("Process-1 Thread-4 exec failed.\n");
    } else {
        while (1) {
            char *plainText = NULL;
            pthread_mutex_lock(&mutex_locks[3]);
            if (isEmptyQueue(&thread_queues[3]) == 0) {
                plainText = dequeue(&thread_queues[3]);
            }
            pthread_mutex_unlock(&mutex_locks[3]);

            if (plainText != NULL) {
                if (strcmp(plainText, END_OF_FILE_CONTROL) == 0) {
                    break;
                } else {
                    /* send to process 1 */

                    String temp;
                    initializeString(&temp, 100);
                    concatString(&temp, plainText);

                    fprintf(outFile, "%s\n", temp.string);
                    fflush(outFile);

                    write(pipefd[WRITE], temp.string, (size_t) temp.size);
                }
            }
        }
        fclose(outFile);
        close(pipefd[WRITE]);
    }
}

void thread_3() {
    FILE *outFile;

    outFile = fopen("process-1-thread-3.txt", "w");

    while (1) {
        /* use mutex to prevent thread collision */
        char *plainText = NULL;
        pthread_mutex_lock(&mutex_locks[2]);
        if (isEmptyQueue(&thread_queues[2]) == 0) {
            plainText = dequeue(&thread_queues[2]);
        }
        pthread_mutex_unlock(&mutex_locks[2]);

        if (plainText != NULL) {
            if (strcmp(plainText, END_OF_FILE_CONTROL) == 0) {
                break;
            } else {
                int i;

                /* subbox process */

                int *plainArray = convertIntArray(plainText);
                for (i = 0; i < 16; i++) {
                    plainArray[i] = get_subbox_val((unsigned char) plainArray[i]);
                }

                String finalArray = convertCharArray(plainArray);
                String temp;
                initializeString(&temp, 100);
                concatString(&temp, finalArray.string);

                fprintf(outFile, "%s\n", temp.string);
                fflush(outFile);

                sendToQueueWithIndex(3, temp.string);
            }
        }
    }
    fclose(outFile);
    sendToQueueWithIndex(3, END_OF_FILE_CONTROL);
}

void thread_2() {
    FILE *outFile;

    outFile = fopen("process-1-thread-2.txt", "w");

    while (1) {
        /* use mutex to prevent thread collision */
        char *plainText = NULL;
        pthread_mutex_lock(&mutex_locks[1]);
        if (isEmptyQueue(&thread_queues[1]) == 0) {
            plainText = dequeue(&thread_queues[1]);
        }
        pthread_mutex_unlock(&mutex_locks[1]);

        if (plainText != NULL) {
            if (strcmp(plainText, END_OF_FILE_CONTROL) == 0) {
                break;
            } else {
                int i;

                /* permutation process */

                int *plainArray = convertIntArray(plainText);
                for (i = 0; i < 8; i++) {
                    swap(&plainArray[i], &plainArray[i + 8]);
                }

                String finalArray = convertCharArray(plainArray);
                String temp;
                initializeString(&temp, 100);
                concatString(&temp, finalArray.string);

                fprintf(outFile, "%s\n", temp.string);
                fflush(outFile);

                sendToQueueWithIndex(2, temp.string);
            }
        }
    }
    fclose(outFile);
    sendToQueueWithIndex(2, END_OF_FILE_CONTROL);
}

void thread_1() {
    String key;
    FILE *outFile;

    key = getKey(key_file_path);
    outFile = fopen("process-1-thread-1.txt", "w");

    while (1) {
        char *plainText = NULL;
        pthread_mutex_lock(&mutex_locks[0]);
        if (isEmptyQueue(&thread_queues[0]) == 0) {
            plainText = dequeue(&thread_queues[0]);
        }
        pthread_mutex_unlock(&mutex_locks[0]);

        if (plainText != NULL) {
            if (strcmp(plainText, END_OF_FILE_CONTROL) == 0) {
                break;
            } else {
                int i;

                int *keyArray = convertIntArray(key.string);
                int *plainArray = convertIntArray(plainText);
                int *cipher = (int *) malloc(16 * sizeof(int));

                /* bitwise xor operation with key and plaintext blocks */

                for (i = 0; i < 16; i++) {
                    cipher[i] = xor(keyArray[i], plainArray[i]);
                }

                String finalArray = convertCharArray(cipher);
                String temp;
                initializeString(&temp, 100);
                concatString(&temp, finalArray.string);

                fprintf(outFile, "%s\n", temp.string);
                fflush(outFile);

                sendToQueueWithIndex(1, temp.string);

            }
        }
    }
    fclose(outFile);
    sendToQueueWithIndex(1, END_OF_FILE_CONTROL);
}

void thread_0() {
    FILE *file, *outFile;
    int c;

    file = fopen(plain_text_path, "r");
    outFile = fopen("./process-1-thread-0.txt", "w");

    String plainText;
    initializeString(&plainText, 200);

    /* read plaintext */
    while ((c = fgetc(file)) != EOF) {
        if (c != '\n') {
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&plainText, temp);
        } else {
            if (plainText.top != -1) {
                concatString(&plainText, "\0");

                String temp;
                initializeString(&temp, 100);
                concatString(&temp, plainText.string);

                fprintf(outFile, "%s\n", temp.string);
                fflush(outFile);

                /* got a line send to thread 1*/
                sendToQueueWithIndex(0, temp.string);

                clearString(&plainText);
            }
        }
    }
    fclose(outFile);
    fclose(file);

    sendToQueueWithIndex(0, END_OF_FILE_CONTROL);
}

void sendToQueueWithIndex(int index, char *element) {
    pthread_mutex_lock(&mutex_locks[index]);

    enqueue(&thread_queues[index], element);

    pthread_mutex_unlock(&mutex_locks[index]);
}

unsigned char get_subbox_val(unsigned char val) {
    unsigned char SUBBOX[256] = {47, 164, 147, 166, 221, 246, 1, 13, 198, 78, 102, 219, 75, 97, 62, 140,
                                 84, 69, 107, 99, 185, 220, 179, 61, 187, 0, 92, 112, 8, 33, 15, 119,
                                 209, 178, 192, 12, 121, 239, 117, 96, 100, 126, 118, 199, 208, 50, 42, 168,
                                 14, 171, 17, 238, 158, 207, 144, 58, 127, 182, 146, 71, 68, 157, 154, 88,
                                 248, 105, 131, 235, 98, 170, 22, 160, 181, 4, 254, 70, 202, 225, 67, 205,
                                 216, 25, 43, 222, 236, 128, 122, 77, 59, 145, 167, 54, 20, 55, 152, 149,
                                 230, 211, 224, 111, 165, 124, 16, 243, 213, 114, 116, 63, 64, 176, 31, 161,
                                 9, 229, 95, 247, 193, 18, 134, 79, 133, 173, 82, 51, 57, 136, 6, 49,
                                 5, 197, 115, 65, 169, 255, 249, 195, 30, 162, 150, 53, 83, 46, 228, 81,
                                 237, 104, 28, 223, 217, 251, 200, 60, 132, 194, 151, 137, 191, 74, 201, 103,
                                 29, 80, 113, 101, 250, 172, 234, 180, 73, 141, 204, 27, 241, 188, 153, 155,
                                 86, 94, 177, 87, 39, 91, 2, 48, 35, 40, 120, 159, 184, 123, 215, 138,
                                 210, 108, 76, 106, 36, 189, 125, 226, 252, 37, 66, 156, 253, 218, 85, 203,
                                 110, 10, 244, 45, 34, 242, 72, 93, 52, 135, 44, 245, 3, 32, 196, 163,
                                 232, 240, 227, 24, 139, 183, 38, 233, 130, 143, 109, 41, 174, 231, 129, 23,
                                 148, 89, 212, 19, 21, 142, 7, 214, 56, 90, 11, 190, 175, 206, 26, 186};
    unsigned char sub_box_val = SUBBOX[val];
    return sub_box_val;
}


/* basic array swap */
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}


/*bitwise xor operation*/
int xor(
int x,
int y
) {
int a = x & y;
int b = ~x & ~y;
int z = ~a & ~b;
return
z;
}


/**
 * array converter splits
 * string with - and adds to int array
 *
 * @param str
 * @return
 */
int *convertIntArray(char *str) {
    char *token;
    int *array;
    int i;
    String temp;
    initializeString(&temp, 100);

    concatString(&temp, str);

    array = (int *) malloc(16 * sizeof(int));

    i = 0;
    token = strtok(temp.string, "-");
    while (token != NULL) {
        array[i] = atoi(token);
        i++;
        token = strtok(NULL, "-");
    }
    return array;
}

/**
 * formats int array to string with delimiter -
 *
 * @param array
 * @return
 */
String convertCharArray(int *array) {
    String temp;
    int i;
    initializeString(&temp, 100);
    for (i = 0; i < 16; i++) {
        char one[10];
        sprintf(one, "%d", array[i]);
        concatString(&temp, one);
        if (i != 15) {
            concatString(&temp, "-\0");
        }
    }
    return temp;
}

void initGlobals() {
    int i, j;

    thread_handles = malloc(THREAD_COUNT * sizeof(pthread_t));

    for (i = 0; i < MUTEX_COUNT; i++) {
        pthread_mutex_init(&mutex_locks[i], NULL);
    }

    for (j = 0; j < QUEUE_COUNT; j++) {
        initializeQueue(&thread_queues[j], MAX_QUEUE_SIZE);
    }
}


/**
 * reads key file and gets key string
 *
 * @param keyFilePath key file absolute path
 * @return string key
 */
String getKey(char *keyFilePath) {
    FILE *file;
    int c;

    file = fopen(keyFilePath, "r");

    String keyString;
    initializeString(&keyString, 200);

    while ((c = fgetc(file)) != EOF) {
        if (c != '\n') {
            char temp[2];
            temp[0] = (char) c;
            temp[1] = '\0';
            concatString(&keyString, temp);
        } else {
            if (keyString.top != -1) {
                concatString(&keyString, "\0");
                break;
                /*clearString(&keyString);*/
            }
        }
    }
    return keyString;
}

void usage() {
    printf("\nUSAGE:       ./a.out <plain_text_file_path> <key_file_path>\n"
                   "Example:     ./a.out ./sample/plain.txt ./sample/key.txt\n");
}


/**
 * String initializer
 * @param string String structure
 * @param initialSize size must be positive
 */
void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

/**
 * concats strings (size auto increased)
 * @param string
 * @param newCharArray
 */
void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}

/**
 * Clear all chars in string but size is same
 * @param string
 */
void clearString(String *string) {

    /* string's chars NULL char "reset string" */

    int i;
    for (i = 0; i <= string->top; i++) {
        string->string[i] = '\0';
    }

    string->top = -1;
}

/**
 * Queue initializer.
 * Programmer gives a initial size, while adding elements array size increased.
 *
 * @param queue
 * @param initialSize
 */
void initializeQueue(Queue *queue, int initialSize) {
    queue->array = (char **) malloc(initialSize * sizeof(char *));
    queue->front = -1;
    queue->rear = -1;
    queue->size = initialSize;
}

/**
 * Adds queue array one element.
 * Array size will increase itself.
 *
 * @param queue
 * @param element
 */
void enqueue(Queue *queue, char *element) {
    String temp;
    initializeString(&temp, 100);
    concatString(&temp, element);
    if (queue->rear == queue->size - 1) {
        queue->size *= 2;
        queue->array = (char **) realloc(queue->array, queue->size * sizeof(char *));
    }
    if (queue->front == -1) {
        queue->front = 0;
    }
    queue->rear++;
    queue->array[queue->rear] = temp.string;
}

/**
 * Deletes an item from Queue's front
 * and returns it.
 *
 * @param queue
 * @return
 */
char *dequeue(Queue *queue) {
    String frontQueue;
    initializeString(&frontQueue, 200);
    if (queue->front == -1 || queue->front > queue->rear) {
        return NULL;
    } else {
        concatString(&frontQueue, queue->array[queue->front]);
        queue->front++;
        return frontQueue.string;
    }
}

/**
 * Returns front item of Queue.
 * But not delete.
 *
 * @param queue
 * @return
 */
char *front(Queue *queue) {
    if (queue->front == -1 || queue->front > queue->rear) {
        return NULL;
    } else {
        return queue->array[queue->front];
    }
}

/**
 * returns true if queue is empty. else false
 *
 * @param queue
 * @return
 */
int isEmptyQueue(Queue *queue) {
    if (queue->front == -1 || queue->front > queue->rear) {
        return 1;
    } else {
        return 0;
    }
}
