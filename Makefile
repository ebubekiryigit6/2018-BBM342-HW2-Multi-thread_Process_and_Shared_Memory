CC = gcc

TARGET_1 = process-1
TARGET_2 = process-2
SOURCE_1 = process-1.c
SOURCE_2 = process-2.c

all:
	$(CC) $(SOURCE_1) -o $(TARGET_1) -ansi -Wall -lpthread
	$(CC) $(SOURCE_2) -o $(TARGET_2) -ansi -Wall -lpthread

.PHONY: clean

clean:
	$(RM) $(TARGET_1)
	$(RM) $(TARGET_2)

