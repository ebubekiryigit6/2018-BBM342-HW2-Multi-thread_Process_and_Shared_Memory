#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


typedef struct {
    char *string;
    int size;
    int top;
} String;

void concatString(String *string, char *newCharArray);

int *convertIntArray(char *str);

void initializeString(String *string, int initialSize);

int bitwiseSum(int p, int q);

String convertCharArray(int *array);


int main(int argc, char **argv) {

    int counter;
    String buffer;
    initializeString(&buffer, 100);

    int sum[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    FILE *outFile;

    outFile = fopen("process-2.txt", "w");

    counter = 0;
    int readPipeMain = atoi(argv[0]);
    while (read(readPipeMain, buffer.string, 100) != 0) {
        int i;
        String temp;
        initializeString(&temp, 100);
        concatString(&temp, buffer.string);

        int *array = convertIntArray(temp.string);
        for (i = 0; i < 16; i++) {
            sum[i] = bitwiseSum(sum[i], array[i]);
        }
        counter++;
        if (counter == 16) {
            String finalArray = convertCharArray(sum);
            fprintf(outFile, "%s\n", finalArray.string);
            fflush(outFile);
        }
    }
    fclose(outFile);
    close(readPipeMain);


    return EXIT_SUCCESS;

}

int bitwiseSum(int p, int q) {
    int result = p + q;
    int carry = result / 256;
    return (result % 256) + carry;
}


void initializeString(String *string, int initialSize) {
    string->string = (char *) malloc(initialSize * sizeof(char));
    string->string[0] = '\0';
    string->size = initialSize;
    string->top = -1;
}

int *convertIntArray(char *str) {
    char *token;
    int *array;
    int i;
    String temp;
    initializeString(&temp, 100);

    concatString(&temp, str);

    array = (int *) malloc(16 * sizeof(int));

    i = 0;
    token = strtok(temp.string, "-");
    while (token != NULL) {
        array[i] = atoi(token);
        i++;
        token = strtok(NULL, "-");
    }
    return array;
}


String convertCharArray(int *array) {
    String temp;
    int i;
    initializeString(&temp, 100);
    for (i = 0; i < 16; i++) {
        char one[10];
        sprintf(one, "%d", array[i]);
        concatString(&temp, one);
        if (i != 15) {
            concatString(&temp, "-\0");
        }
    }
    return temp;
}


void concatString(String *string, char *newCharArray) {

    /* concats two strings */

    int i;
    for (i = 0; newCharArray[i] != '\0'; i++) {
        if (string->top == string->size - 1) {
            string->size *= 2;
            string->string = (char *) realloc(string->string, string->size * sizeof(char));
        }
        string->top++;
        string->string[string->top] = newCharArray[i];
    }

    if (string->top == string->size - 1) {
        string->size *= 2;
        string->string = (char *) realloc(string->string, string->size * sizeof(char));
    }

    string->string[string->top + 1] = '\0';
}