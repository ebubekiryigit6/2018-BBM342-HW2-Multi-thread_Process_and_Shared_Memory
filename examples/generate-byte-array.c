#include <stdio.h>
#include <stdlib.h>
#include <time.h>

size_t ARRAY_SIZE = 16;

unsigned char *gen_rdm_bytes (size_t num_bytes);

void print_byte_array(unsigned char *data, size_t size);

void generate_byte_example();


unsigned char *gen_rdm_bytes (size_t num_bytes)
{
  unsigned char *bytes = malloc (num_bytes);
  size_t i;

  for (i = 0; i < num_bytes; i++)
  {
    bytes[i] = rand ();
  }

  return bytes;
}


void generate_byte_example() {
	unsigned char *plain = gen_rdm_bytes(ARRAY_SIZE);
	unsigned char *key = gen_rdm_bytes(ARRAY_SIZE);

	printf("Plain data is: ");
	print_byte_array(plain, ARRAY_SIZE);

	printf("Key is: ");
	print_byte_array(key, ARRAY_SIZE);	
}

void print_byte_array(unsigned char *data, size_t size) {
	for(size_t i = 0; i < size - 1; i++ ) {
		printf("%d-", data[i]);
	}
	printf("%d\n", data[size - 1]);	
}

int main() {
	for(size_t i = 0; i < 10; i++) {
		generate_byte_example();
		printf("---------------------\n");
	}
	return 0;
}
